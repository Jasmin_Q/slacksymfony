<?php

namespace App\DataTransferObject\Slack;

use JoliCode\Slack\Api\Model\ObjsChannel;

class SlackChannel
{
    private $name;
    private $id;

    public function __construct(ObjsChannel $channel)
    {
        $this->name = $channel->getName();
        $this->id = $channel->getId();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getId(): ?string
    {
        return $this->id;
    }
}