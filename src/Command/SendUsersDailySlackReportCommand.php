<?php

namespace App\Command;

use ApiPlatform\Core\Serializer\InputOutputMetadataTrait;
use App\DataTransferObject\Slack\SlackChannel;
use App\Service\SlackService;
use JoliCode\Slack\Api\Model\ObjsChannel;
use Swift_Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Templating\EngineInterface;

class SendUsersDailySlackReportCommand extends Command
{
    /** @var Swift_Mailer */
    private $mailer;
    private $slack;
    private $twig;

    public function __construct(
        Swift_Mailer $mailer,
        SlackService $slack,
        EngineInterface $twig
    ) {
        parent::__construct();

        $this->mailer   = $mailer;
        $this->slack    = $slack;
        $this->twig     = $twig;
    }

    protected function configure()
    {
        $this
            ->setName('slack:dsm:send')
            ->setDescription('Send report for every DailyStandupMeeting channel');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $listOfChannelObjects = $this->slack->getChannelList()->getChannels();

        $slackChannels = [];
        $configuration = [
            'general' => [
                'guzinajasmin91@gmail.com'
            ],

        ];

        /** @var ObjsChannel $channelObject */
        foreach ($listOfChannelObjects as $channelObject) {
            // abuse ValueObjects or DTOs
            $slackChannel = new SlackChannel($channelObject);
            if (array_key_exists($slackChannel->getName(), $configuration)) {
                $slackChannels[] = $slackChannel;
            }
        }

        $output->writeln(sprintf('<comment>> Starting DailyStandupMeeting report for </comment><info>%s</info><comment> channels:</comment>', count($slackChannels)));

        /** @var SlackChannel $slackChannel */
        foreach ($slackChannels as $slackChannel) {
            $recipients = $configuration[$slackChannel->getName()];

            if (!empty($recipients)) {
                $messages = $this->slack->getChannelHistory($slackChannel->getId());

                if (!empty($messages)) {
                    $result = $this->sendReport($recipients, $messages, $slackChannel);

                    $output->writeln(sprintf('  - Sending report for channel: <info>%-30s</info> <info>%s</info>',
                        $slackChannel->getName(),
                        $result ? 'OK' : 'ERROR'
                    ));
                } else {
                    $output->writeln(sprintf('  - Sending report for channel: <info>%-30s</info> <error>%s</error>',
                        $slackChannel->getName(),
                        'Nothing to send'
                    ));
                }
            }
        }

        $output->writeln('<comment>> Done. </comment>');
    }

    private function sendReport(array $recipients, array $messages, SlackChannel $channel): int
    {
        $today = new \DateTime('now');

        $body = $this->twig->render('mail/slack_report.html.twig', [
            'channel' => [
                'name' => $channel->getName(),
                'messages' => $messages
            ],
            'today' => $today
        ]);

        $message = new \Swift_Message(sprintf(
            '[DSM] %s (%s)',
            $channel->getName(),
            $today->format('Y-m-d')
        ));

        $message
            ->setFrom('send@example.com')
            ->setTo($recipients)
            ->setBody($body, 'text/html');


        return $this->mailer->send($message);
    }
}