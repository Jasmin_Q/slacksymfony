<?php

namespace App\Service;

use Carbon\Carbon;
use JoliCode\Slack\Api\Client;
use JoliCode\Slack\ClientFactory;

class SlackService
{
    private const TOKEN = 'xoxp-840801550711-841156406518-838533560356-09bd124ccb32b4b6fdc667c790373eb7';

    /** @var Client */
    private $slack;

    public function __construct()
    {
        $this->slack = ClientFactory::create(self::TOKEN);
    }

    public function getChannelList()
    {
        $queryParameters = [
            'exclude_members' => true,

        ];
        return $this->slack->channelsList();
    }

    public function getChannelHistory(string $id): array
    {
        $startOfDayTimestamp = Carbon::parse('today')->startOfDay()->getTimestamp();

        // This method require your token to have the scope "users:read"
        $response = $this->slack->conversationsHistory([
            'channel'   => $id,
            'limit'     => 100,
            'oldest'    => (string) $startOfDayTimestamp,
        ]);

        return $response->getMessages();
    }
}
