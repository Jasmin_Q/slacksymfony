#Install

Run composer install:

    composer install

Setup your environment:
   - Insert your list of channel and recipients in the file SlackSymfony/src/Command/SendUsersDailySlackReportCommand.php
   - Insert your token in the file src/Service/SlackService.php
   
  
Run docker-compose build 

    docker-compose build
    
Run docker-compose up -d

    docker-compose up -d 
    
    
Run Symfony Command 

    bin/console slack:dsm:send
 


  

